# SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT

matplotlib==3.1.3
pandas==1.0.1
